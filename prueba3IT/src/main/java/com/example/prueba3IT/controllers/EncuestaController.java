package com.example.prueba3IT.controllers;

import com.example.prueba3IT.models.Encuesta;
import com.example.prueba3IT.services.EncuestasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/encuestas")
@CrossOrigin(origins = "http://localhost:4200/")
public class EncuestaController {

    @Autowired
    EncuestasService encuestasService;

    @PostMapping
    public ResponseEntity guardarEncuestas(@RequestBody Encuesta encuesta){
        return new ResponseEntity(encuestasService.guardarEncuesta(encuesta), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity obtenerEncuestas(){
        return new ResponseEntity(encuestasService.obtenerEncuestas(), HttpStatus.OK);
    }
}
