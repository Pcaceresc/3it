package com.example.prueba3IT.repository;

import com.example.prueba3IT.models.Encuesta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EncuestaRepository extends JpaRepository<Encuesta,String> {
}
