package com.example.prueba3IT.services;

import com.example.prueba3IT.models.Encuesta;
import com.example.prueba3IT.repository.EncuestaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EncuestasSerivceImp implements EncuestasService{

    @Autowired
    EncuestaRepository encuestaRepository;

    @Override
    public Encuesta guardarEncuesta(Encuesta encuesta) {
        try{
            return encuestaRepository.save(encuesta);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Encuesta> obtenerEncuestas() {
        return encuestaRepository.findAll();
    }
}
