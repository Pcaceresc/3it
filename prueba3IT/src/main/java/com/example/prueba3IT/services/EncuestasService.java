package com.example.prueba3IT.services;

import com.example.prueba3IT.models.Encuesta;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface EncuestasService {

    Encuesta guardarEncuesta(Encuesta encuesta);

    List<Encuesta> obtenerEncuestas();
}
