package com.example.prueba3IT;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Prueba3ItApplication {

	public static void main(String[] args) {
		SpringApplication.run(Prueba3ItApplication.class, args);
	}

}
