package com.example.prueba3IT.models;

import jakarta.persistence.*;

import java.io.Serializable;
@Entity
@Table()
public class Encuesta implements Serializable {

    @Id
    private String mail;
    private String estiloMusical;

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getEstiloMusical() {
        return estiloMusical;
    }

    public void setEstiloMusical(String estiloMusical) {
        this.estiloMusical = estiloMusical;
    }
}
