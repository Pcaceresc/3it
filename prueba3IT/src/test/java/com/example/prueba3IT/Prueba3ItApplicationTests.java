package com.example.prueba3IT;

import com.example.prueba3IT.controllers.EncuestaController;
import com.example.prueba3IT.models.Encuesta;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class Prueba3ItApplicationTests {

	@Autowired
	private EncuestaController encuestaController;

	@Test
	void contextLoads() {
	}

	@Test
	public void obtenerEncuestas() {
		ResponseEntity respuesta;
		respuesta = encuestaController.obtenerEncuestas();
		assertEquals(HttpStatus.OK,respuesta.getStatusCode());
	}

	@Test
	public void guardarEncuesta() {
		ResponseEntity respuesta;
		Encuesta encuesta = new Encuesta();
		encuesta.setMail("prueba");
		encuesta.setEstiloMusical("Rock");
		respuesta = encuestaController.guardarEncuestas(encuesta);
		assertEquals(HttpStatus.CREATED,respuesta.getStatusCode());
	}

	@Test
	@ExceptionHandler(value = Exception.class)
	public void guardarEncuestaError() {
		ResponseEntity respuesta;
		Encuesta encuesta = new Encuesta();
		encuesta.setEstiloMusical("Rock");
		respuesta = encuestaController.guardarEncuestas(encuesta);
	}



}
