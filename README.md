# Prueba 3IT

Pasos para levantar el proyecto:

- Clonar repositorio en tu local

**FRONT**

- Abrir una terminal e ingresar comando `cd Front`
- Luego `npm install`
- Luego `ng serve`
- La aplicación estará disponible en localhost:4200

**BACK** (Abrir carpeta back en otra ventana)

- Abrir una terminal e ingresar comando `cd prueba3IT`
- Luego `./mvnw spring-boot:run`
- La aplicación estará disponible en localhost:8080
